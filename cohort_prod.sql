WITH customers AS (
                SELECT
                    a.email AS user_id,
                    date_trunc('month', min(a.createddate)) AS cohort_month
                FROM
                    netsuite_beta.transaction a
                WHERE
                    _type = 'SalesOrder'
                    AND a.department__name = 'Website'                 
                GROUP BY 1
            ),

orders AS  (
                SELECT
                    a.email AS user_id,
                    date_trunc('month', a.createddate) AS months_active,
                    sum(a.subtotal - a.taxtotal) AS revenue,
                    count(a.tranid) AS orders
                FROM
                    netsuite_beta.transaction a
                WHERE _type = 'SalesOrder'
                     AND a.department__name = 'Website'
                    
                GROUP BY 1, 2               
             ),

cohort_size AS (
                select
                cohort_month,
                count(DISTINCT(c.user_id)) as cohort_size 
                FROM customers A
                JOIN orders C ON A.user_id = C.user_id
                group by 1
                order by 1
        
    )



SELECT
    c.cohort_month,
    o.months_active AS months_actual,
    (
        pg_catalog.rank() OVER(
            PARTITION BY c.cohort_month
            ORDER BY
                o.months_active
        ) - 1
    ) AS month_rank,
    s.cohort_size,
    count(DISTINCT c.user_id) AS active_customers,
    ROUND(active_customers/cohort_size::float, 3) as engagement, 
    sum(orders) as total_orders,
    ROUND(total_orders/active_customers::float, 2) as OPAC,
    ROUND(sum(o.revenue), 2) AS total_revenue,
    ROUND(total_revenue/total_orders, 2) as aov,
    SUM(total_revenue) over (PARTITION BY c.cohort_month order by months_actual ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) as cum_rev,
    cum_rev/cohort_size as LTV 
from customers c
join orders o  ON c.user_id = o.user_id
JOIN cohort_size s ON s.cohort_month = c.cohort_month 
GROUP BY 1, 2,  4 
ORDER BY 1, 2 